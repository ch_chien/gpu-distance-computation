#ifndef GPU_CPF_KERNEL_CU
#define GPU_CPF_KERNEL_CU

#include <stdio.h>
#include <stdlib.h>
#include <cstdio>
#include <iostream>
#include <iomanip>
#include <cstring>
#include <cuda.h>
#include <cuda_runtime.h>

#include "gpu_kernels.h"
#include "definitions.hpp"

namespace ParallelDistCompute{

template<int numOfThreads>
__global__
void
gpu_CPF_kernel(
    int    pivotSize,
    int    numOfTotalPivotBatches,
    int    numOfPivotBatchesPerBlock,
    //int    pivotBatchCount,
    float *dev_pivots, 
    float *dev_queries,
    int   *dev_closest_pindx,
    long long *clocks )
{
    extern __shared__ double sdata[];

    //> Thread Ids and Block Ids
    const int tx = threadIdx.x;
    const int bx = blockIdx.x;

    //> Shared memory ptrs
    float* sPivots   = (float*)sdata;

    int          i = 0;
    int          p = 0;
    float        rQueries[DATA_DIM] = {0.0};
    float        rDist = 0.0;
    float        rPrevDist = 1000000;
    unsigned int closest_pivot_index;
    unsigned int pivot_batches_index = 0;
    
    //> Read query data from global memory to register file
    rQueries[0] = dev_queries[bx*DATA_DIM*NUM_OF_THREADS + 2*tx];       //> x
    rQueries[1] = dev_queries[bx*DATA_DIM*NUM_OF_THREADS + 2*tx + 1];   //> y
    //> End of reading query data to registers

    //> the timer
    long long start = clock64();

    #pragma unroll
    for (p = 0; p < numOfTotalPivotBatches; p++) {
        pivot_batches_index = p*numOfPivotBatchesPerBlock*DATA_DIM*NUM_OF_THREADS;

        //> Read pivots data from global memory to shared memory   
        #pragma unroll
        for (i = 0; i < numOfPivotBatchesPerBlock; i++) {

            //> If the index of the pivot data the thread accesses is outside the pivot data size boundary
            if ( pivot_batches_index + i*DATA_DIM*NUM_OF_THREADS + 2*tx < pivotSize*DATA_DIM ) {
                sPivots[i*DATA_DIM*NUM_OF_THREADS + 2*tx]     = dev_pivots[pivot_batches_index + i*DATA_DIM*NUM_OF_THREADS + 2*tx];          //> x
                sPivots[i*DATA_DIM*NUM_OF_THREADS + 2*tx + 1] = dev_pivots[pivot_batches_index + i*DATA_DIM*NUM_OF_THREADS + 2*tx + 1];      //> y
            }        
        }
        __syncthreads();
        //> End of reading pivots data to shared memory

        //> Compute the squared L2-norm distance
        #pragma unroll
        for (i = 0; i < NUM_OF_THREADS*numOfPivotBatchesPerBlock; i++) {
            rDist  = 0.0;
            rDist += (sPivots[2*i] - rQueries[0])*(sPivots[2*i] - rQueries[0]);
            rDist += (sPivots[2*i + 1] - rQueries[1])*(sPivots[2*i + 1] - rQueries[1]);
            if (rDist < rPrevDist) {
                rPrevDist = rDist;
                closest_pivot_index = p*numOfPivotBatchesPerBlock*NUM_OF_THREADS + i;
            }
        }
    }

    if( tx == 0 ) {
        clocks[bx] = clock64() - start;
        printf("bx = %d, clock cycles = %llu\n", bx, clock64() - start);
    }

    //> Write the closest pivot index back to the global memory
    dev_closest_pindx[bx*numOfThreads + tx] = closest_pivot_index;
}


extern "C"
void gpu_closest_pivot_finder(
    int device_id,
    int numOfBlocks,
    int pivotSize,
    int numOfTotalPivotBatches,
    int numOfPivotBatchesPerBlock,
    float *dev_pivots, float *dev_queries, 
    int *dev_closest_pindx,
    long long *clocks )
{
    dim3 grid(numOfBlocks, 1, 1);
    dim3 threads(NUM_OF_THREADS, 1, 1);

    //> Shared Memory
    int shmem = 0;
    shmem += NUM_OF_THREADS * DATA_DIM * numOfPivotBatchesPerBlock * sizeof(float);        //> sPivots

    //> Get max. dynamic shared memory on the GPU
    int nthreads_max, shmem_max = 0;
    cudacheck( cudaDeviceGetAttribute(&nthreads_max, cudaDevAttrMaxThreadsPerBlock, device_id) );
    #if CUDA_VERSION >= 9000
    cudacheck( cudaDeviceGetAttribute (&shmem_max, cudaDevAttrMaxSharedMemoryPerBlockOptin, device_id) );
    if (shmem <= shmem_max) {
        cudacheck( cudaFuncSetAttribute(gpu_CPF_kernel<NUM_OF_THREADS>, cudaFuncAttributeMaxDynamicSharedMemorySize, shmem) );
    }
    #else
    cudacheck( cudaDeviceGetAttribute (&shmem_max, cudaDevAttrMaxSharedMemoryPerBlock, device_id) );
    #endif    // CUDA_VERSION >= 9000

    //> Check if the use of the shared memory exceeds the limited size (in Volta, shared memory can be configured up to 96KB per SM)
    if ( shmem > shmem_max ) {
        printf("ERROR: kernel %s requires too many threads or too much shared memory\n", __func__);
    }

    //> Kernel input arguments
    void *kernel_args[] = {&pivotSize, &numOfTotalPivotBatches, &numOfPivotBatchesPerBlock, &dev_pivots, &dev_queries, &dev_closest_pindx, &clocks};

    //> Lauch a GPU kernel
    cudacheck( cudaLaunchKernel((void*)gpu_CPF_kernel<NUM_OF_THREADS>, grid, threads, kernel_args, shmem, NULL) );
}

}

#endif