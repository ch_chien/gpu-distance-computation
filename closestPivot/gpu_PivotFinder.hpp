#ifndef GPU_PIVOTFINDER_HPP
#define GPU_PICOTFINDER_HPP

#include <cmath>
#include <math.h>
#include <fstream>
#include <iostream>
#include <string.h>
#include <assert.h>
#include <vector>
#include <chrono>
#include <cuda_runtime.h>
#include <cuda_runtime_api.h>

#include "gpu_kernels.h"
#include "definitions.hpp"

namespace ParallelDistCompute {

class PivotFinder_GPU {
    int device_id;
    int pivotSize;
    int querySize;
    //int pivotBatchCount;        //> Number of batches (or warps in GPU) used for pivot data
    //int queryBatchCount;        //> Number of batches (or warps in GPU) used for query data
    //int numOfQueriesPerBatch;   //> Number of queries per block in GPU

    float *pivots;
    float *queries;

    float *dev_pivots;
    float *dev_queries;
    int   *dev_closest_pindx;
    long long *clk_data;
    long long *host_clk_data;

    int   *closest_pindx_GPU;
    int   *closest_pindx_CPU;

  public:

    //> Timing variable
    float time_gpu_pivot_finder_kernel;
    float time_cpu_pivot_finder;
    cudaEvent_t start, stop;

    PivotFinder_GPU(int, int, int);
    ~PivotFinder_GPU();

    //> Member Functions
    void preprocessing(float* dataPointer);

    //> GPU
    void GPU_ClosestPivotFinder();

    //> CPU
    void CPU_ClosestPivotFinder();

    //> Check Correctness of the GPU results
    void check_gpu();

    /*
    void read_array_from_file(std::string filename, T **rd_data, int first_dim, int second_dim);
    void write_array_to_file(std::string filename, T *wr_data, int first_dim, int second_dim);
    */
  private:
    //> Measure clock time
    int peak_clk;
    float max_block_time;
    float block_time;
    float sumOfClkTime;

    int numOfBlocks;
    int numOfTotalPivotBatches;
    int numOfPivotBatchesPerBlock;
};

// ==================================== Constructor ===================================
//> Define parameters used by member functions and allocate 2d arrays dynamically
// ====================================================================================
PivotFinder_GPU::PivotFinder_GPU (int device, int numOfPivots, int numOfQueries) {
    device_id = device;
    pivotSize = numOfPivots;
    querySize = numOfQueries;
    //numOfQueriesPerBatch = 128;

    //> GPU kernel settings
    // const int numOfThreads = 128;                                          //> (tunable) assign 4 warps per block
    const int pivotBatchCount = ceil((float)(pivotSize*DATA_DIM) / (float)(NUM_OF_THREADS));
    const int queryBatchCount = ceil((float)(querySize*DATA_DIM) / (float)(NUM_OF_THREADS));
    numOfBlocks               = queryBatchCount;                              //> One batch of queries (4 warps) is a block
    numOfPivotBatchesPerBlock = 1;                                            //> (tunable)
    //> end of kernel parameters

    numOfTotalPivotBatches    = ceil((float)pivotBatchCount / (float)numOfPivotBatchesPerBlock);

    //> Initialization
    max_block_time = 0.0;
    block_time = 0.0;
    sumOfClkTime = 0.0;

    //> get device attribute
    cudaError_t err = cudaDeviceGetAttribute(&peak_clk, cudaDevAttrClockRate, device_id);
    printf(" - peak clock rate: %d (kHz)\n", peak_clk);
    
    //> Initialize arrays storing pivot and query data
    //> CPU
    pivots          = new float[pivotSize * DATA_DIM];
    queries         = new float[querySize * DATA_DIM];
    host_clk_data   = (long long *)malloc( numOfBlocks*sizeof(long long) );
    // for (int i = 0; i < numOfBlocks; i++)
    //     host_clk_data[i] = 0;

    //> GPU
    cudacheck( cudaMalloc((void**)&dev_pivots,          pivotSize*DATA_DIM*sizeof(float)) );
    cudacheck( cudaMalloc((void**)&dev_queries,         querySize*DATA_DIM*sizeof(float)) );
    cudacheck( cudaMalloc((void**)&dev_closest_pindx,   querySize*sizeof(int)) );
    cudacheck( cudaMalloc((void**)&clk_data,            numOfBlocks*sizeof(long long)) );

    //> Arrays storing closest indices returned from GPU and CPU
    closest_pindx_GPU = new int[querySize];
    closest_pindx_CPU = new int[querySize];

    //> Timing variables
    time_gpu_pivot_finder_kernel = 0;
    time_cpu_pivot_finder = 0;

    //> cuda events used for timings
    cudacheck( cudaEventCreate(&start) );
    cudacheck( cudaEventCreate(&stop) );
}

// ========================= preprocessing ==========================
// Initialize 2d arrays
// ==================================================================
void PivotFinder_GPU::preprocessing(float* dataPointer) {

    unsigned int dataPointer_cnter = 0;

    //> Assign data from the dataset to pivots    
    for (unsigned int i = 0; i < pivotSize; i++) {
        for (unsigned int j = 0; j < DATA_DIM; j++) {
            pivots(i,j) = dataPointer[dataPointer_cnter];
            dataPointer_cnter++;
        }
    }

    //> Assign data from the dataset to queries   
    for (unsigned int i = 0; i < querySize; i++) {
        for (unsigned int j = 0; j < DATA_DIM; j++) {
            queries(i,j) = dataPointer[dataPointer_cnter];
            dataPointer_cnter++;
        }
    }

#if CHECK_ASSERTION_FAIL
        //std::cout << "dataPointer_cnter: " << dataPointer_cnter << std::endl;
        assert(dataPointer_cnter != (pivotSize + querySize)*DATA_DIM);
#endif

    //> The use of CUDA API *cudaMemset* sets the value (which is 0 here for all arrays) of global memory in GPU for arrays we need to store in.
    cudacheck( cudaMemset(dev_pivots,         0, pivotSize*DATA_DIM*sizeof(float)) );
    cudacheck( cudaMemset(dev_queries,        0, querySize*DATA_DIM*sizeof(float)) );
    cudacheck( cudaMemset(dev_closest_pindx,  0, querySize*sizeof(int)) );
    // cudacheck( cudaMemset(clk_data,           0, numOfBlocks*sizeof(long long)) );

    //> The use of CUDA API *cudaMemcpy* copies the memory in CPU (host) to the global memory in GPU (device).
    //> We only copy pivots and queries data from CPU to GPU. The closest_pindx array takes the data from GPU kernel computation
    //> so no data is required to be copied from the CPU side.
    cudacheck( cudaMemcpy(dev_pivots,   pivots,   pivotSize*DATA_DIM*sizeof(float), cudaMemcpyHostToDevice) );
    cudacheck( cudaMemcpy(dev_queries,  queries,  querySize*DATA_DIM*sizeof(float), cudaMemcpyHostToDevice) );
}

// ============================= GPU Closest Pivot Finder =====================================
// Description TODO
// ============================================================================================
void PivotFinder_GPU::GPU_ClosestPivotFinder()
{
    //> Start timing the GPU kernel
	cudacheck( cudaEventRecord(start) );

    //> This is the GPU processing
    // gpu_closest_pivot_finder( device_id, pivotSize, querySize, dev_pivots, dev_queries, dev_closest_pindx);
    gpu_closest_pivot_finder( device_id, numOfBlocks, pivotSize, numOfTotalPivotBatches, numOfPivotBatchesPerBlock, dev_pivots, dev_queries, dev_closest_pindx, clk_data );

	cudacheck( cudaEventRecord(stop) );
	cudacheck( cudaEventSynchronize(stop) );
	cudacheck( cudaEventElapsedTime(&time_gpu_pivot_finder_kernel, start, stop) );

    //> Copy data from Device (GPU) to Host (CPU)
    cudacheck( cudaMemcpy(closest_pindx_GPU,  dev_closest_pindx,  querySize*sizeof(int),         cudaMemcpyDeviceToHost) );
    // cudacheck( cudaMemcpy(host_clk_data,      clk_data,           numOfBlocks*sizeof(long long), cudaMemcpyDeviceToHost) );

    //> Print out the GPU timing
    std::string time_out_str = std::to_string(time_gpu_pivot_finder_kernel) + " (ms)";
    LOG_GPU_TIMEIMGS(time_out_str)
    //std::cout << "GPU Timing: " << std::setprecision(10) << time_gpu_pivot_finder_kernel << " (ms)" << std::endl;

#if MEASURE_CLOCK_CYCELS
    //> Print out the clock timing in the GPU kernel
    for (int b = 0; b < numOfBlocks; b++) {
        // std::cout << host_clk_data[b]-DELAY_CLK_CYCLE << std::endl;
        block_time = (host_clk_data[b]-DELAY_CLK_CYCLE) / (float)(peak_clk);
	    if (block_time > max_block_time) {
	        max_block_time = block_time;
	    }
        //printf("measured clock cycles: %ld, elapsed time: %fms\n", host_clk_data[b], host_clk_data[b]/(float)peak_clk);
        sumOfClkTime += block_time;
    }
    printf(" - Average batch elapsed time: %f (ms)\n", sumOfClkTime / (float)numOfBlocks);
    printf(" - Max block time = %f (ms)\n", max_block_time);
#endif

    //> Print out the results for debugging use
#if PRINT_GPU_RESULTS
    for (int i = 0; i < 10; i++) std::cout << closest_pindx_GPU[i] << "\t";
    std::cout << std::endl;
#endif
}

void PivotFinder_GPU::CPU_ClosestPivotFinder()
{
    //> Set the timer for CPU
    auto begin = std::chrono::high_resolution_clock::now();

    float dist     = 0.0;
    float prevDist = 1000000;
    unsigned closest_pIndex_in_process = 0;
    for (int q = 0; q < querySize; q++) {
        prevDist = 1000000;
        for (int p = 0; p < pivotSize; p++) {

            //> Reset the distance value to zero
            dist = 0.0;

            for (int k = 0; k < DATA_DIM; k++) {
                dist += (pivots(p,k) - queries(q,k)) * (pivots(p,k) - queries(q,k));
            }

            if (dist < prevDist) {
                prevDist = dist;
                closest_pIndex_in_process = p;
            }
        }
        closest_pindx_CPU[q] = closest_pIndex_in_process;
    }

    //> End the timer for CPU
    auto end = std::chrono::high_resolution_clock::now();
    time_cpu_pivot_finder = std::chrono::duration_cast<std::chrono::microseconds>(end-begin).count();

    //> Print out the CPU timing
    // std::cout << "CPU Timing: " << std::setprecision(10) << (time_cpu_pivot_finder)/1000.0 << " (ms)" << std::endl;
    std::string time_out_str = std::to_string((time_cpu_pivot_finder)/1000.0) + " (ms)";
    LOG_CPU_TIMEIMGS(time_out_str)

    //> Print out the results for debugging use
#if (PRINT_CPU_RESULTS)
    for (int i = 0; i < 10; i++) std::cout << closest_pindx_CPU[i] << "\t";
    std::cout << std::endl;
#endif
}

// ================== Check The Correctness of the GPU Computation Results ====================
// Check the consistency of the closest pivot index for each qiery returned by GPU and CPU to
// see whether the GPU computation is correct.
// ============================================================================================
void PivotFinder_GPU::check_gpu()
{
    bool correct = true;
    for (int i = 0; i < querySize; i++) {  
        if (closest_pindx_GPU[i] != closest_pindx_CPU[i]) {
            std::cout << i << "\t";
        }
        correct &= (closest_pindx_GPU[i] == closest_pindx_CPU[i]);
    }
    std::cout << std::endl;

    //> Print out the message
    if (correct) 
        LOG_INFOR_MESG("Results returned by GPU and CPU are CONSISTENT!");
    else 
        LOG_ERROR("Results returned by GPU and CPU are INCONSISTENT!");
}

// ===================================== Destructor =======================================
// Free all the 2d dynamic arrays allocated in the constructor
// ========================================================================================
PivotFinder_GPU::~PivotFinder_GPU () {
    //> Free CPU Memory
    delete[] pivots;
    delete[] queries;

    delete[] closest_pindx_GPU;
    delete[] closest_pindx_CPU;
    free(host_clk_data);

    //> Free GPU Memory
    cudacheck( cudaFree(dev_pivots) );
    cudacheck( cudaFree(dev_queries) );
    cudacheck( cudaFree(dev_closest_pindx) );
    cudacheck( cudaFree(clk_data) );

    cudacheck( cudaEventDestroy(start) );
    cudacheck( cudaEventDestroy(stop) );
}

}

#endif