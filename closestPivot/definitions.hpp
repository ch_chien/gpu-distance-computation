//> Macros definitions
#define PROJ_DIR                std::string("/users/cchien3/data/cchien3/MyBitBucket/gpu-nearest-neighbor/")
#define DATASET_FILE_NAME       PROJ_DIR + std::string("datasets/2d_1.024_10pow7_single.bin")

//> Settings
#define DATA_DIM                (2)
#define NUM_OF_THREADS          (128)
#define MEASURE_CLOCK_CYCELS    (false)

//> indices for CPU
#define pivots(i,j)             pivots[(i) * DATA_DIM + (j)]
#define queries(i,j)            queries[(i) * DATA_DIM + (j)]

//> Some checkings
#define CHECK_CUDA_DEVICE       (true)
#define CHECK_ASSERTION_FAIL    (false)

//> DEBUGGING
#define PRINT_GPU_RESULTS       (false)
#define PRINT_CPU_RESULTS       (false)


//> Delayed clock cycles
#define DELAY_CLK_CYCLE         (10000)

//> CUDA error check
#define cudacheck( a )  do { \
                            cudaError_t e = a; \
                            if(e != cudaSuccess) { \
                                printf("\033[1;31m"); \
                                printf("Error in %s:%d %s\n", __func__, __LINE__, cudaGetErrorString(e)); \
                                printf("\033[0m"); \
                            }\
                        } while(0)

#define LOG_GPU_TIMEIMGS(time_msg)      printf("\033[1;35m[GPU TIME] %s\033[0m\n", std::string(time_msg).c_str() );
#define LOG_CPU_TIMEIMGS(time_msg)      printf("\033[1;35m[CPU TIME] %s\033[0m\n", std::string(time_msg).c_str() );
#define LOG_INFOR_MESG(info_msg)        printf("\033[1;32m[INFO] %s\033[0m\n", std::string(info_msg).c_str() )
#define LOG_FILE_ERROR(err_msg)         printf("\033[1;31m[ERROR] File %s not found!\033[0m\n", std::string(err_msg).c_str() )
#define LOG_ERROR(err_msg)              printf("\033[1;31m[ERROR] %s\033[0m\n", std::string(err_msg).c_str() )
#define LOG_DATA_LOAD_ERROR(err_msg)    printf("\033[1;31m[DATA LOAD ERROR] %s not loaded successfully!\033[0m\n", std::string(err_msg).c_str() )
#define LOG_PRINT_HELP_MESSAGE          printf("Usage: ./magmaHC-main [options] [path]\n\n" \
                                               "options:\n" \
                                               "  -h, --help        show this help message and exit\n" \
                                               "  -d, --directory   repository directory, e.g. /home/chchien/Homotopy-Continuation-Tracker-on-GPU/GPU-HC/\n");