#ifndef GPU_KERNELS_H
#define GPU_KERNELS_H

#include<stdio.h>
#include<assert.h>
#include<cuda.h>
#include<cuda_runtime_api.h>

namespace ParallelDistCompute {

extern "C" {
void gpu_closest_pivot_finder(
    int device_id,
    int numOfBlocks,
    int pivotSize,
    int numOfTotalPivotBatches,
    int numOfPivotBatchesPerBlock,
    float *dev_pivots, float *dev_queries, 
    int *dev_closest_pindx,
    long long *clocks
);
}

}

#endif //> GPU_KERNELS_H