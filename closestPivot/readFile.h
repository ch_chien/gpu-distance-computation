#ifndef readFile_cuh_
#define readFile_cuh_
// ============================================================================
// Header file for reading input file
//
// Modifications
//    Chien  21-05-03:   Initially Created.
//
// ============================================================================
#include <cstdio>
#include <iostream>
#include <iomanip>
#include <cstring>
#include "./definitions.hpp"

namespace readWriteFiles {

  bool
  readBinary_Dataset(float*& dataPointer, unsigned long int const datasetSize, unsigned int const dimension)
  {
    bool success = false;

    // open dataset file
    std::ifstream inputFileStream(DATASET_FILE_NAME, std::ios::binary);
    if(!inputFileStream.is_open())
    {
        LOG_FILE_ERROR(DATASET_FILE_NAME);
        exit(-1);
    }

    // read first integer, which should be the same as the dimension
    unsigned int data_dimension;
    inputFileStream.read((char*)&data_dimension, sizeof(int)); // reads first int, which gives us dimension of the following point
    inputFileStream.seekg(0, std::ios::end);

    // return error if dimension of dataset is different from input
    if (dimension != data_dimension)
    {
        std::cout << "Error: Dataset dimension different from input dimension" << std::endl;
        std::cout << "  - Input dimension: " << dimension << std::endl;
        std::cout << "  - Data dimension: " << data_dimension << std::endl;
        return success;
    }

    // get number of points in the dataset
    std::ios::pos_type streamPosition = inputFileStream.tellg();
    std::size_t byteSize = (std::size_t)streamPosition;
    unsigned long int fileDatasetSize = (unsigned long int)(byteSize / (dimension*sizeof(float) + sizeof(int)));

    // return error if dataset size requested is larger than what we have
    if (datasetSize > fileDatasetSize)
    {
        std::cout << "Error: Requested dataset size larger than dataset contains" << std::endl;
        std::cout << "  - Requested size: " << datasetSize << std::endl;
        std::cout << "  - True size: " << fileDatasetSize << std::endl;
        return success;
    }

    // declare dataset pointer
    dataPointer = new float[datasetSize * dimension * sizeof(float)];
    
    // read the elements of the dataset
    inputFileStream.seekg(0, std::ios::beg);
    for(std::size_t i = 0; i < datasetSize; i++)
    {
        inputFileStream.seekg(sizeof(int), std::ios::cur);
        inputFileStream.read((char*)(dataPointer + i*dimension), dimension*sizeof(float));
    }
    inputFileStream.close();

    // if we made it here, successful load
    success = true;

    return success;
  }
}

#endif
