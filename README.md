# GPU Parallel Distance Computation #

# 1. Introduction
This project attempts to find the closest pivot data from a set of query data using GPU. The process starts by spliting the input data into two sets: *(i)* pivots and *(ii)* qieries. For each query, it finds the index of the closest pivot by computing the distance between the two. This involves two levels of parallelisms: *(i)* all queries finding the closest pivot can be done in parallel, and *(ii)* for each query, distance computation to all pivots can be done in parallel. The data the code takes in is now in two-dimensional, and several parameters in the GPU kernel can be tuned to get the optimal performances.<br />

# 2. Requirements on Brown University CCV
```
(1) cmake/3.26.3-xi6h36u
(2) cuda/11.8.0-lpttyok (or higher like 12.1.1)
```

# 3. How to use the code
(1) clone the repo
```bash
git clone <git clone https://ch_chien@bitbucket.org/ch_chien/gpu-distance-computation.git>
```
(2) under the repo folder, create a 'build' directory and enter to it
```bash
mkdir build && cd build
```
(3) create a make file
```bash
cmake ..
```
(4) compile the entire code
```bash
make -j
```
(5) enter the bin foler
```bash
cd bin
```
(6) run the code by specifically giving input arguments 
```bash
./closestPivot-main -p 3200 -q 10240
```

## 4. Sonme Notices:
Change the absolute directory of the repository in ``definitions.hpp``. There are also settings in that file used as macros for the code. <br>

