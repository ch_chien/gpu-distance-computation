#include <cmath>
#include <fstream>
#include <iterator>
#include <iostream>
#include <string.h>
#include <vector>
#include <stdint.h>

#include <cuda_runtime.h>
#include <cuda_runtime_api.h>

// ============================================================================
//> Finding the closest pivot given a set of queries.
//> MAIN FUNCTION
//
//> Modifications
//    Chien  23-01-14:   Initially Created.
//    Chien  24-11-18:   Tried to add clock cycle counter.
//
// ============================================================================

//> Header files
#include "./closestPivot/readFile.h"
#include "./closestPivot/gpu_kernels.h"
#include "./closestPivot/gpu_PivotFinder.hpp"
#include "./closestPivot/definitions.hpp"

void printMessage()
{
    std::cerr << "===============================================================================================================\n";
    std::cerr << "Usage: ./cuDistCompute-main <input-argument> <command>\n\n";
    std::cerr << "<input-argument> <command>\n"
                    "       -p     <unsigned integer>     # (or --pivots)  : number of pivots\n"
                    "       -q     <unsigned integer>     # (or --queries) : number of queries\n"
                    "       -h                            # (or --help)    : print this help message\n\n";
    std::cerr << "----------------------- NOTICE -----------------------\n";
    std::cerr << "Order matters!!\n";
    std::cerr << "----------------------- Examples ---------------------\n";
    std::cerr << "./closestPivots-main -p 3200 -q 10240 \n";
    std::cerr << "===============================================================================================================\n";
}

//> ====================================== MAIN ====================================
int main(int argc, char **argv)
{
	--argc; ++argv;
    std::string arg;
    int argIndx = 0;
    int argTotal = 6;
    unsigned int numOfPivots  = 0;
    unsigned int numOfQueries = 0;

    if (argc) {
        arg = std::string(*argv);
        if (arg == "-h" || arg == "--help") {
            printMessage();
            exit(1);
        }
        else if (argc <= argTotal) {
            while(argIndx <= argTotal-1) {
                if (arg == "-p" || arg == "--pivots") {
                    argv++;
                    arg = std::string(*argv);
                    numOfPivots = std::stoi(arg);
                    argIndx += 2;
                }
                else if (arg == "-q" || arg == "--qeuries") {
                    argv++;
                    arg = std::string(*argv);
                    numOfQueries = std::stoi(arg);
                    argIndx++;
                    break;
                }
                else {
                    std::cerr<<"Invalid input arguments! See examples: \n";
                    printMessage();
                    exit(1);
                }
                argv++;
                arg = std::string(*argv);
            }
        }
        else if (argc > argTotal) {
            std::cerr<<"too many arguments! See examples: \n";
            printMessage();
            exit(1);
        }
    }
    else {
        printMessage();
        exit(1);
    }
    std::cout << std::endl;

    //> Check cuda device to make sure there is a GPU
    int gpu_id = 0;
#if CHECK_CUDA_DEVICE
	cudacheck( cudaSetDevice(gpu_id) );
#endif

    //> Read the dataset
    unsigned long int datasetSize = numOfPivots + numOfQueries;
    float* dataPointer = NULL;
    if (!readWriteFiles::readBinary_Dataset(dataPointer, datasetSize, DATA_DIM)) {
        LOG_DATA_LOAD_ERROR(DATASET_FILE_NAME);
        return 0;
    }

    //> Class constructor
    ParallelDistCompute::PivotFinder_GPU CPF(gpu_id, numOfPivots, numOfQueries);

    //> Preprocessing: CPU and GPU arrays allocations
	CPF.preprocessing( dataPointer );

    //> Execute the GPU kernel
    CPF.GPU_ClosestPivotFinder();

    //> Do it on CPUs for validation
    CPF.CPU_ClosestPivotFinder();

    //> Check whether the results returned from CPU and GPU are consistent
    CPF.check_gpu();

    delete dataPointer;
    return 0;
}
