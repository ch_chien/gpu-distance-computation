#ifndef GPU_TEST_KERNEL_CU
#define GPU_TEST_KERNEL_CU

#include <stdio.h>
#include <stdlib.h>
#include <chrono>
#include <cstdio>
#include <iostream>
#include <iomanip>
#include <cstring>
#include <cuda.h>
#include <cuda_runtime.h>

#include "gpu_kernels.h"
#include "indices.hpp"

namespace ParallelDistCompute{

__global__
void
gpu_SimpleMath_kernel( 
    int threadSize
)
{
    //extern __shared__ double sdata[];

    //> Thread Ids and Block Ids
    const int tx = threadIdx.x;
    const int bx = blockIdx.x;

    float rA[32]  = {0.5};
    float rB[32]  = {1.0};

    for (int k = 0; k < 5000; k++) {
      rB[tx] *= rA[tx];
      rB[tx] += k;
    }
}


extern "C"
void gpu_GPU_understander_kernel(
    int device_id,
    int threadSize, 
    int blockSize )
{
    //> GPU kernel parameters
    const int numOfThreads = 32;
    const int numOfBlocks  = 1024;
    //> end of kernel parameters

    printf("Number of threads = %d\n", threadSize);
    printf("Number of blocks  = %d\n", blockSize);

    dim3 grid(numOfThreads, 1, 1);
    dim3 threads(numOfBlocks, 1, 1);

    //> Shared Memory
    int shmem = 0;
    //shmem += numOfThreads * DATA_DIM * numOfPivotBatchesPerBlock * sizeof(float);        //> sPivots

    //> Get max. dynamic shared memory on the GPU
    int nthreads_max, shmem_max = 0;
    cudacheck( cudaDeviceGetAttribute(&nthreads_max, cudaDevAttrMaxThreadsPerBlock, device_id) );
    #if CUDA_VERSION >= 9000
    cudacheck( cudaDeviceGetAttribute (&shmem_max, cudaDevAttrMaxSharedMemoryPerBlockOptin, device_id) );
    if (shmem <= shmem_max) {
        cudacheck( cudaFuncSetAttribute(gpu_SimpleMath_kernel, cudaFuncAttributeMaxDynamicSharedMemorySize, shmem) );
    }
    #else
    cudacheck( cudaDeviceGetAttribute (&shmem_max, cudaDevAttrMaxSharedMemoryPerBlock, device_id) );
    #endif    // CUDA_VERSION >= 9000

    //> Check if the use of the shared memory exceeds the limited size (in Volta, shared memory can be configured up to 96KB per SM)
    if ( shmem > shmem_max ) {
        printf("ERROR: kernel %s requires too many threads or too much shared memory\n", __func__);
    }

    //> Kernel input arguments
    void *kernel_args[] = {&threadSize};

    auto begin = std::chrono::high_resolution_clock::now();

    //> Lauch a GPU kernel
    cudacheck( cudaLaunchKernel((void*)gpu_SimpleMath_kernel, grid, threads, kernel_args, shmem, NULL) );

    auto end = std::chrono::high_resolution_clock::now();
    float time_gpu = std::chrono::duration_cast<std::chrono::microseconds>(end-begin).count();
    std::cout << "GPU Timing: " << std::setprecision(10) << time_gpu/1000.0 << " (ms)" << std::endl;

}

}

#endif