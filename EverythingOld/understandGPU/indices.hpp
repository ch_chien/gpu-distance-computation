#ifndef INDICES_HPP
#define INDICES_HPP
// macros for flexible axis

#define DATA_DIM             (2)
#define DEBUG                (0)

// cpu
#define pivots(i,j)          pivots[(i) * DATA_DIM + (j)]
#define queries(i,j)         queries[(i) * DATA_DIM + (j)]


// gpu


#endif // INDICES_HPP