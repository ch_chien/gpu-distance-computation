#ifndef GPU_KERNELS_H
#define GPU_KERNELS_H

#include<stdio.h>
#include<assert.h>
#include<cuda.h>
#include<cuda_runtime_api.h>

namespace ParallelDistCompute {
#define cudacheck( a )  do { \
                            cudaError_t e = a; \
                            if(e != cudaSuccess) { \
                                printf("\033[1;31m"); \
                                printf("Error in %s:%d %s\n", __func__, __LINE__, cudaGetErrorString(e)); \
                                printf("\033[0m"); \
                            }\
                        } while(0)

extern "C" {
void gpu_GPU_understander_kernel(
    int device_id,
    int numOfThreads,
    int numOfBlocks
    //int pivotSize, int querySize,
    //float *dev_pivots, float *dev_queries, 
    //int *dev_closest_pindx
);
}

}

#endif //> GPU_KERNELS_H