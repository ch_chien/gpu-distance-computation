#ifndef GPU_CLASS_HELPER_HPP
#define GPU_CLASS_HELPER_HPP

#include <cmath>
#include <math.h>
#include <fstream>
#include <iostream>
#include <string.h>
#include <assert.h>
#include <vector>
#include <chrono>
#include <cuda_runtime.h>
#include <cuda_runtime_api.h>

#include "gpu_kernels.h"
#include "indices.hpp"

namespace ParallelDistCompute {

class GPU_Helper {
    int device_id;
    int threadSize;
    int blockSize;

    /*float *pivots;
    float *queries;

    float *dev_pivots;
    float *dev_queries;
    int   *dev_closest_pindx;

    int   *closest_pindx_GPU;
    int   *closest_pindx_CPU;*/

  public:

    //> Timing variable
    float time_gpu;
    float time_cpu;
    cudaEvent_t start, stop;

    GPU_Helper(int, int, int);
    ~GPU_Helper();

    //> Member Functions
    //void preprocessing(float* dataPointer);

    //> GPU
    void GPU_KernelLauncher();

    //> CPU
    void CPU_ClosestPivotFinder();

    //> Check Correctness of the GPU results
    void check_gpu();
};

// ==================================== Constructor ===================================
//> Define parameters used by member functions and allocate 2d arrays dynamically
// ====================================================================================
GPU_Helper::GPU_Helper (int device, int numOfThreads, int numOfBlocks) {
    device_id = device;
    threadSize = numOfThreads;
    blockSize  = numOfBlocks;
    
/*    //> Initialize arrays storing pivot and query data
    //> CPU
    pivots    = new float[pivotSize * DATA_DIM];
    queries   = new float[querySize * DATA_DIM];

    //> GPU
    cudacheck( cudaMalloc((void**)&dev_pivots,          pivotSize*DATA_DIM*sizeof(float)) );
    cudacheck( cudaMalloc((void**)&dev_queries,         querySize*DATA_DIM*sizeof(float)) );
    cudacheck( cudaMalloc((void**)&dev_closest_pindx,   querySize*sizeof(int)) );

    //> Arrays storing closest indices returned from GPU and CPU
    closest_pindx_GPU = new int[querySize];
    closest_pindx_CPU = new int[querySize];
*/
    //> Timing variables
    time_gpu = 0;
    time_cpu = 0;

    //> cuda events used for timings
    cudacheck( cudaEventCreate(&start) );
    cudacheck( cudaEventCreate(&stop) );
}

// ========================= preprocessing ==========================
// Initialize 2d arrays
// ==================================================================
/*void GPU_Helper::preprocessing(float* dataPointer) {

    unsigned int dataPointer_cnter = 0;

    //> Assign data from the dataset to pivots    
    for (unsigned int i = 0; i < pivotSize; i++) {
        for (unsigned int j = 0; j < DATA_DIM; j++) {
            pivots(i,j) = dataPointer[dataPointer_cnter];
            dataPointer_cnter++;
        }
    }

    //> Assign data from the dataset to queries   
    for (unsigned int i = 0; i < querySize; i++) {
        for (unsigned int j = 0; j < DATA_DIM; j++) {
            queries(i,j) = dataPointer[dataPointer_cnter];
            dataPointer_cnter++;
        }
    }

    if (DEBUG) {
        //std::cout << "dataPointer_cnter: " << dataPointer_cnter << std::endl;
        assert(dataPointer_cnter != (pivotSize + querySize)*DATA_DIM);
    }

    //> The use of CUDA API *cudaMemset* sets the value (which is 0 here for all arrays) of global memory in GPU for arrays we need to store in.
    cudacheck( cudaMemset(dev_pivots,         0, pivotSize*DATA_DIM*sizeof(float)) );
    cudacheck( cudaMemset(dev_queries,        0, querySize*DATA_DIM*sizeof(float)) );
    cudacheck( cudaMemset(dev_closest_pindx,  0, querySize*sizeof(int)) );

    //> The use of CUDA API *cudaMemcpy* copies the memory in CPU (host) to the global memory in GPU (device).
    //> We only copy pivots and queries data from CPU to GPU. The closest_pindx array takes the data from GPU kernel computation
    //> so no data is required to be copied from the CPU side.
    cudacheck( cudaMemcpy(dev_pivots,   pivots,   pivotSize*DATA_DIM*sizeof(float), cudaMemcpyHostToDevice) );
    cudacheck( cudaMemcpy(dev_queries,  queries,  querySize*DATA_DIM*sizeof(float), cudaMemcpyHostToDevice) );
}*/

// ============================= GPU Kernel Launcher =====================================
// Description TODO
// =======================================================================================
void GPU_Helper::GPU_KernelLauncher()
{
    //> Start timing the GPU kernel
	//cudacheck( cudaEventRecord(start) );

    auto begin = std::chrono::high_resolution_clock::now();

    //> This is the GPU processing
    gpu_GPU_understander_kernel( device_id, threadSize, blockSize );

    auto end = std::chrono::high_resolution_clock::now();
    time_gpu = std::chrono::duration_cast<std::chrono::microseconds>(end-begin).count();

	//cudacheck( cudaEventRecord(stop) );
	//cudacheck( cudaEventSynchronize(stop) );
	//cudacheck( cudaEventElapsedTime(&time_gpu, start, stop) );

    //> Copy data from Device (GPU) to Host (CPU)
    //cudacheck( cudaMemcpy(closest_pindx_GPU,  dev_closest_pindx,  querySize*sizeof(int),   cudaMemcpyDeviceToHost) );

    //> Print out the GPU timing
    //std::cout << "GPU Timing: " << std::setprecision(10) << time_gpu*1000 << " (ms)" << std::endl;

    //> Print out the results for debugging use
	/*if (DEBUG) {
        for (int i = 0; i < 10; i++) std::cout << closest_pindx_GPU[i] << "\t";
        std::cout << std::endl;
    }*/
}

/*
void GPU_Helper::CPU_ClosestPivotFinder()
{
    //> Set the timer for CPU
    auto begin = std::chrono::high_resolution_clock::now();

    float dist     = 0.0;
    float prevDist = 1000000;
    unsigned closest_pIndex_in_process = 0;
    for (int q = 0; q < querySize; q++) {
        prevDist = 1000000;
        for (int p = 0; p < pivotSize; p++) {

            //> Reset the distance value to zero
            dist = 0.0;

            for (int k = 0; k < DATA_DIM; k++) {
                dist += (pivots(p,k) - queries(q,k)) * (pivots(p,k) - queries(q,k));
            }

            if (dist < prevDist) {
                prevDist = dist;
                closest_pIndex_in_process = p;
            }
        }
        closest_pindx_CPU[q] = closest_pIndex_in_process;
    }

    //> End the timer for CPU
    auto end = std::chrono::high_resolution_clock::now();
    time_cpu_pivot_finder = std::chrono::duration_cast<std::chrono::microseconds>(end-begin).count();

    //> Print out the CPU timing
    std::cout << "CPU Timing: " << std::setprecision(10) << (time_cpu_pivot_finder)/1000.0 << " (ms)" << std::endl;

    //> Print out the results for debugging use
    if (DEBUG) {
        for (int i = 0; i < 10; i++) std::cout << closest_pindx_CPU[i] << "\t";
        std::cout << std::endl;
    }
}*/

// ================== Check The Correctness of the GPU Computation Results ====================
// Check the consistency of the closest pivot index for each qiery returned by GPU and CPU to
// see whether the GPU computation is correct.
// ============================================================================================
/*void GPU_Helper::check_gpu()
{
    bool correct = true;
    for (int i = 0; i < querySize; i++) { 
    //for (int i = 65; i < 128; i++) {  
        if (closest_pindx_GPU[i] != closest_pindx_CPU[i]) {
            std::cout << i << "\t";
        }
        correct &= (closest_pindx_GPU[i] == closest_pindx_CPU[i]);
    }
    std::cout << std::endl;

    //> Print out the message
    if (correct) std::cout << "Results returned by GPU and CPU are CONSISTENT!" << std::endl;
    else std::cout << "Results returned by GPU and CPU are INCONSISTENT!" << std::endl;
}*/

// ===================================== Destructor =======================================
// Free all the 2d dynamic arrays allocated in the constructor
// ========================================================================================
GPU_Helper::~GPU_Helper () {
    //> Free CPU Memory
    /*delete[] pivots;
    delete[] queries;

    delete[] closest_pindx_GPU;
    delete[] closest_pindx_CPU;

    //> Free GPU Memory
    cudacheck( cudaFree(dev_pivots) );
    cudacheck( cudaFree(dev_queries) );
    cudacheck( cudaFree(dev_closest_pindx) );*/

    //cudacheck( cudaEventDestroy(start) );
    //cudacheck( cudaEventDestroy(stop) );
}

}

#endif    // GPU_CANNY_HPP