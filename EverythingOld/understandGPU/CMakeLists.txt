
set (control_sources  
   gpu_test_kernel.cu
   gpu_kernels.h
   gpu_class_helper.hpp
   indices.hpp
   readFile.h
)

include_directories(${CMAKE_CURRENT_BINARY_DIR})
include_directories( "/users/cchien3/data/cchien3/magma-2.5.4/include" )
include_directories( "/users/cchien3/data/cchien3/magma-2.5.4/testing" )
#include_directories( "/users/cchien3/data/cchien3/magma/control" )
#include_directories( "/users/cchien3/data/cchien3/magma/magmablas" )
add_library( understandGPU ${control_sources} )
target_link_libraries(understandGPU 
        -L${CUDAPATH}/lib cublas cudart cusparse
        -L/gpfs/runtime/opt/openblas/0.3.7/lib openblas
        -L/usr/lib64 pthread
        )

#set_target_properties(understandGPU PROPERTIES CUDA_PTX_COMPILATION ON)
set_target_properties(understandGPU PROPERTIES CUDA_SEPARABLE_COMPILATION ON)

target_compile_options(understandGPU   PRIVATE $<$<COMPILE_LANGUAGE:CUDA>: 
                                       --generate-code arch=compute_70,code=sm_70
#                                       --maxrregcount 128
                                       >)
