#include <cmath>
#include <fstream>
#include <iterator>
#include <iostream>
#include <string.h>
#include <vector>
#include <stdint.h>

#include <cuda_runtime.h>
#include <cuda_runtime_api.h>

// ============================================================================
//> Finding the closest pivot given a set of queries.
//> MAIN FUNCTION
//
//> Modifications
//    Chiang-Heng Chien  23-01-14:   Initially Created.
//
// ============================================================================

//> Header files
#include "./understandGPU/readFile.h"
#include "./understandGPU/gpu_kernels.h"
#include "./understandGPU/gpu_class_helper.hpp"
#include "./understandGPU/indices.hpp"

std::string projDirectory    = "/users/cchien3/data/cchien3/MyBitBucket/gpu-nearest-neighbor/";
std::string datasetFileName  = projDirectory + "datasets/2d_1.024_10pow7_single.bin";

void printMessage()
{
    std::cerr << "===============================================================================================================\n";
    std::cerr << "Usage: ./cuDistCompute-main <input-argument> <command>\n\n";
    std::cerr << "<input-argument> <command>\n"
                    "       -p     <unsigned integer>     # (or --pivots)  : number of pivots\n"
                    "       -q     <unsigned integer>     # (or --queries) : number of queries\n"
                    "       -h                            # (or --help)    : print this help message\n\n";
    std::cerr << "----------------------- NOTICE -----------------------\n";
    std::cerr << "Order matters!!\n";
    std::cerr << "----------------------- Examples ---------------------\n";
    std::cerr << "./closestPivots-main -p 3200 -q 10240 \n";
    std::cerr << "===============================================================================================================\n";
}

//> ====================================== MAIN ====================================
int main(int argc, char **argv)
{
	--argc; ++argv;
    std::string arg;
    int argIndx = 0;
    int argTotal = 6;
    unsigned int numOfThreads  = 0;
    unsigned int numOfBlocks = 0;

    if (argc) {
        arg = std::string(*argv);
        if (arg == "-h" || arg == "--help") {
            printMessage();
            exit(1);
        }
        else if (argc <= argTotal) {
            while(argIndx <= argTotal-1) {
                if (arg == "-p" || arg == "--pivots") {
                    argv++;
                    arg = std::string(*argv);
                    numOfThreads = std::stoi(arg);
                    argIndx += 2;
                }
                else if (arg == "-q" || arg == "--qeuries") {
                    argv++;
                    arg = std::string(*argv);
                    numOfBlocks = std::stoi(arg);
                    argIndx++;
                    break;
                }
                else {
                    std::cerr<<"Invalid input arguments! See examples: \n";
                    printMessage();
                    exit(1);
                }
                argv++;
                arg = std::string(*argv);
            }
        }
        else if (argc > argTotal) {
            std::cerr<<"too many arguments! See examples: \n";
            printMessage();
            exit(1);
        }
    }
    else {
        printMessage();
        exit(1);
    }
    std::cout << std::endl;

    //> Check cuda device to make sure there is a GPU
    int gpu_id = 0;
    #if 1
	cudacheck( cudaSetDevice(gpu_id) );
    #endif  

    //> Class constructor
    ParallelDistCompute::GPU_Helper GPU_Diver(gpu_id, numOfThreads, numOfBlocks);

    GPU_Diver.GPU_KernelLauncher();

    //> Execute the GPU kernel
    //GPU_Diver.GPU_ClosestPivotFinder();

    //> Do it on CPUs for validation
    //GPU_Diver.CPU_ClosestPivotFinder();

    //> Check whether the results returned from CPU and GPU are consistent
    //GPU_Diver.check_gpu();

    //delete dataPointer;
    return 0;
}
