#include <iostream>
#include <fstream>
#include <chrono>
#include <ostream>
#include <vector>
#include <cmath>
#include <unordered_set>
// ============================================================================
// main function
//
// Modifications
//    Chien  21-05-07:   Originally created
//    Chien  21-05-23:   Add manual inputs and higer dimensions of data
//
// ============================================================================
// -- read input file --
#include "cuDistCompute/readFile.h"

// -- input info --
#include "cuDistCompute/input-info.h"

// -- gpu computation --
#include "cuDistCompute/gpu_compute.cuh"

//std::string projDirectory = "/gpfs/data/bkimia/cchien3/MyBitBucket/gpu-nearest-neighbor/";
std::string projDirectory = "/users/cchien3/data/cchien3/MyBitBucket/gpu-nearest-neighbor/";

int main(int argc, char **argv) {
  --argc; ++argv;
  std::string arg;
  int argIndx = 0;
  int argTotal = 6;
  unsigned long int numberOfPoints = 0;
  int numberOfPoints_toGPU = 0;
  int repetitive_time = 0;

  if (argc) {
    arg = std::string(*argv);
    if (arg == "-h" || arg == "--help") {
      cmdInputs::print_usage();
      exit(1);
    }
    else if (argc <= argTotal) {
      while(argIndx <= argTotal-1) {
        if (arg == "-n" || arg == "--number") {
          argv++;
          arg = std::string(*argv);
          numberOfPoints = std::stoi(arg);
	  argIndx += 2;
          //break;
        }
	else if (arg == "-k" || arg == "--times") {
	  argv++;
	  arg = std::string(*argv);
	  repetitive_time = std::stoi(arg);
	  argIndx++;
	  break;
        }
        else {
          std::cerr<<"invalid input arguments! See examples: \n";
          cmdInputs::print_usage();
          exit(1);
        }
	argv++;
	arg = std::string(*argv);
      }
    }
    else if (argc > argTotal) {
      std::cerr<<"too many arguments! See examples: \n";
      cmdInputs::print_usage();
      exit(1);
    }
  }
  else {
    cmdInputs::print_usage();
    exit(1);
  }

  std::cout << std::endl;

  // User Defined Parameters
  //std::string filename = projDirectory + "datasets/32d_1.024_10pow5_single.bin"; // to dataset
  //std::string filename = projDirectory + "datasets/128d_1.024_10pow5_single.bin"; // to dataset
  std::string filename = projDirectory + "datasets/2d_1.024_10pow7_single.bin"; // to dataset
  unsigned int dimension = 2; // dimension of the dataset
  unsigned long int datasetSize = numberOfPoints;
  int ref_Point = 0;

  // read the dataset
  float* dataPointer = NULL; // make sure to delete pointer
  bool success_read = readWriteFiles::readBinary_Dataset(filename.c_str(), dataPointer, datasetSize, dimension);
  if (!success_read) return 0;  

  

  // -- gpu version --
  //cuDistComputeWrapper::gpu_compute(dataPointer, dimension, datasetSize, ref_Point, repetitive_time);
  cuDistComputeWrapper::gpu_compute_dist_per_thread(dataPointer, dimension, datasetSize, ref_Point, repetitive_time);
  
  delete dataPointer;

  return 0;
}
