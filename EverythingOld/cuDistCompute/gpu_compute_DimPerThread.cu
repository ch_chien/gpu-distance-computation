#ifndef gpu_compute_cu
#define gpu_compute_cu
// =======================================================================
// GPU version of computing distances between two points - specifications
// of header file gpu_compute.cuh
//
// Modifications
//    Chien  21-05-10:   Originally Created
//
// =======================================================================
#include <stdio.h>
#include <stdlib.h>
#include <cstdio>
#include <chrono>
#include <iostream>
#include <iomanip>
#include <cstring>
#include <unordered_set>

// -- magma --
#include "flops.h"
#include "magma_v2.h"
#include "magma_lapack.h"
#include "magma_internal.h"
#undef max
#undef min
#include "magma_templates.h"
#include "sync.cuh"
#undef max
#undef min
#include "shuffle.cuh"
#undef max
#undef min
#include "batched_kernel_param.h"

// -- cuda included --
#include <cuda.h>
#include <cuda_runtime.h>

// -- headers --
#include "gpu_compute.cuh"
#include "gpu_kernels.h"

// -- cpu computation --
//#include "cpu_compute.h"

namespace cuDistComputeWrapper {

  void Cvt2GpuArray(
    float* dataPointer, unsigned int const dimension, unsigned int const batchCount,
    int ref_Point, float *h_A, float *h_B)
  {
    for (int i = 0; i < batchCount; i++) {
      for (unsigned int d = 0; d < dimension; d++)
      {
        (h_A + i * dimension)[d] = dataPointer[ref_Point*dimension + d];
        (h_B + i * dimension)[d] = dataPointer[(i+1)*dimension + d];
      }
    }
  }

  real_Double_t gpu_compute(
    float* dataPointer, unsigned int const dimension, unsigned long int const dataSize,
    int ref_Point, int repetitive_time)
  {
    std::unordered_set<unsigned int> set_gpu;

    magma_init();
    magma_print_environment();

    real_Double_t  gflops, gpu_perf, gpu_time;
    real_Double_t  time_cpu;
    float          *h_A, *h_B;
    magmaFloat_ptr d_A, d_B;
    magma_int_t N, lda, ldb, ldda, lddb, sizeA, sizeB;
    long long *clk_data;
    long long *host_clk_data;
    // -- write required clock-cycle time of each block --
    std::ofstream sm_time_file;
    sm_time_file.open("/users/cchien3/data/cchien3/MyBitBucket/gpu-nearest-neighbor-analyze/block_clkcyc_time.txt");
    if (!sm_time_file.is_open())
       std::cout<<"sm time file cannot be opened!"<<std::endl;

    magma_int_t batchCount;
    float **dA_array = NULL;
    float **dB_array = NULL;

    N = dimension;
    batchCount = dataSize-1;

    magma_queue_t my_queue;    // magma queue variable, internally holds a cuda stream and a cublas handle
    magma_device_t cdev;       // variable to indicate current gpu id

    magma_getdevice( &cdev );
    magma_queue_create( cdev, &my_queue );     // create a queue on this cdev
    int peak_clk = 1;	       // measure clock time
    int device = 0;	       // device id used in get attribute instruction
    
    lda    = N;
    ldb    = lda;
    ldda   = magma_roundup( N, 32 );  // multiple of 32 by default
    lddb   = ldda;
    gflops = ( FLOPS_SGETRF( N, 1 ) + FLOPS_SGETRS( N, 1 ) ) * batchCount / 1e9;

    sizeA = lda*batchCount;
    sizeB = ldb*batchCount;

    magma_smalloc_cpu( &h_A, sizeA );
    magma_smalloc_cpu( &h_B, sizeB );

    magma_smalloc( &d_A, ldda*batchCount );
    magma_smalloc( &d_B, lddb*batchCount );
    
    // -- clock and gpu clock peak frequency --
    host_clk_data = (long long *)malloc(batchCount*sizeof(long long));
    cudaError_t err = cudaDeviceGetAttribute(&peak_clk, cudaDevAttrClockRate, device);
    err = cudaMalloc(&clk_data, batchCount*sizeof(long long));

    magma_malloc( (void**) &dA_array,    batchCount * sizeof(float*) );
    magma_malloc( (void**) &dB_array,    batchCount * sizeof(float*) );

    // -- convert the data pointer to h_A and h_B --
    Cvt2GpuArray(dataPointer, dimension, batchCount, ref_Point, h_A, h_B);

    magma_ssetmatrix( N, batchCount, h_A, lda, d_A, ldda, my_queue );
    magma_ssetmatrix( N, batchCount, h_B, ldb, d_B, lddb, my_queue );

    magma_sset_pointer( dA_array, d_A, ldda, 0, 0, ldda, batchCount, my_queue );
    magma_sset_pointer( dB_array, d_B, lddb, 0, 0, lddb, batchCount, my_queue );

    std::cout<<" -- original -- "<<std::endl;
    int s = 0;
    magma_sprint(N, 1, h_A + s * ldb, lda);
    magma_sprint(N, 1, h_B + s * ldb, lda);

    // ===================================================================
    // magma GPU cgesv batched solver
    // ===================================================================
    std::cout<<"GPU computing ..."<<std::endl;
    gpu_time = magma_sync_wtime( my_queue );
    kernel_vecDistCompute_DimPerThread( N, batchCount, dA_array, ldda, dB_array, my_queue, repetitive_time, clk_data);
    //kernel_vecDistCompute_DistPerThread( N, batchCount, dA_array, ldda, dB_array, my_queue, repetitive_time, clk_data);
    gpu_time = magma_sync_wtime( my_queue ) - gpu_time;
    gpu_perf = gflops / gpu_time;

    // -- check returns from the kernel --
    magma_sgetmatrix( N, batchCount, d_A, ldda, h_A, lda, my_queue );
    magma_sgetmatrix( N, batchCount, d_B, lddb, h_B, ldb, my_queue );
    magma_sprint(N, 1, h_A + s * ldb, lda);
    magma_sprint(N, 1, h_B + s * ldb, lda);
    
    // -- transfer back to cpu; used to measure clock cycles and convert it into time --
    err = cudaMemcpy(host_clk_data, clk_data, batchCount*sizeof(long long), cudaMemcpyDeviceToHost);
    //if (err != cudaSuccess) {printf("cuda err: %d at line %d\n", (int)err, __LINE__); return 1;}
    printf("peak clock rate: %dkHz\n", peak_clk);
    
    float max_block_time = 0.0, block_time = 0.0;
    int batchid = 0;
    for (int b = 0; b < batchCount; b++) {
        block_time = host_clk_data[b]/(float)peak_clk;
	sm_time_file << b << "\t" << block_time << "\n";
	//if (block_time > max_block_time) {
	//   max_block_time = block_time;
        //   batchid = b;
	//}
	//printf("measured clock cycles: %ld, elapsed time: %fms\n", host_clk_data[b], host_clk_data[b]/(float)peak_clk);
    }
    sm_time_file.close();
    
    // -- store all non-zero indices --
    //for (int i = 0; i < batchCount; i++) {
    //  if ((h_B + i * ldb)[0] != 0) {
    //    set_gpu.insert(i+1);
    //  }
    //}

    //for (auto const &i: set_cpu) {
    //      std::cout << i << " ";
    //}
    //std::cout<<std::endl;
    //for (auto const &i: set_gpu) {
    //      std::cout << i << " ";
    //}
    //std::cout<<std::endl;
    
    // -- cpu compute --
    //time_cpu = magma_sync_wtime( my_queue );
    //std::unordered_set<unsigned int> set_cpu = cpu_version(dataPointer,dimension, dataSize, repetitive_time);
    //time_cpu = magma_sync_wtime( my_queue ) - time_cpu;
    
    // -- verify whether it is numerically correct for both cpu and gpu -- 
    //std::cout << "Number of points in set in cpu: " << set_cpu.size() << std::endl;
    //std::cout << "Number of points in set in gpu: " << set_gpu.size() << std::endl;
    
    //bool okay = (set_cpu.size() == set_gpu.size());
    bool okay = 1;

    printf("%% DataSize   Dimensions   Repetitions   CPU time (msec)   GPU time (msec)   Residual\n");
    printf("%%=============================================================================\n");
    std::cout<<"    "<<(long long) dataSize<<"         "<<(long long) N<<"          "<<repetitive_time<<"         ";
    std::cout<< time_cpu*1000<<"           "<< gpu_time*1000<<"            "<<(okay ? "ok" : "failed")<<std::endl;

    free( host_clk_data );
    magma_queue_destroy( my_queue );
    magma_free_cpu( h_A );
    magma_free_cpu( h_B );

    magma_free( d_A );
    magma_free( d_B );
    magma_free( dA_array );
    magma_free( dB_array );
    cudaFree( clk_data );
    fflush( stdout );
    printf( "\n" );
    magma_finalize();

    return gpu_time*1000;
  }
} // end of namespace

#endif
