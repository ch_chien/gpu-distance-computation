#ifndef gpu_kernels_cu
#define gpu_kernels_cu
// =======================================================================
// CUDA kernels specification from header file gpu_kernels.h
//
// Modifications
//    Chien  21-05-10:   Originally Created
//    Chien  21-05-23;   Add repetition time
//    Chien  21-05-27:   Edit: One thread computes one dimension of distance
//    Chien  21-06-02:   Add clock counter to count time of each block (warp)
//
// =======================================================================
#include <stdio.h>
#include <stdlib.h>
#include <cstdio>
#include <iostream>
#include <iomanip>
#include <cstring>

// -- magma --
#include "flops.h"
#include "magma_v2.h"
#include "magma_lapack.h"
#include "magma_internal.h"
#undef max
#undef min
#include "magma_templates.h"
#include "sync.cuh"
#undef max
#undef min
#include "shuffle.cuh"
#undef max
#undef min
#include "batched_kernel_param.h"

// -- cuda included --
#include <cuda.h>
#include <cuda_runtime.h>

// -- headers --
#include "gpu_kernels.h"

namespace cuDistComputeWrapper {

  template<int N>
  __global__ void
  kernel_cuDistCompute(
    float** dA_array, magma_int_t ldda,
    float** dB_array, int repetitive_time, long long *clocks)
  {
    long long start=clock64();
    const int tx = threadIdx.x;
    const int batchid = blockIdx.x;

    float* dA = dA_array[batchid];
    float* dB = dB_array[batchid];
    float rA = 0.0;
    float rB = 0.0;

    extern __shared__ float zdata[];
    float *sD = (float*)(zdata);
    float *sX = sD + N;
    sX[0] = 0;

    // -- read data from global memory to registers --
    rA = dA[tx];
    rB = dB[tx];
    
    //long long start=clock64();
    //long long start;
    #pragma unroll
    for(int k = 0; k < repetitive_time; k++) { 
        //start=clock64();
	sD[tx] = (rA - rB) * (rA - rB);
	__syncthreads();
	magma_sum_reduce< N >(tx, sD);
	__syncthreads();

        sX[0] = sqrt(sD[0]);
        __syncthreads(); 
    }
    //if(tx == 0) {
      //clocks[batchid] = clock64()-start;
    //}
    

    dB[0] = sX[0];
    // -- do not count the time of thresholding --
    //if(sX[0] < 1.4) {
    //  dB[0] = sX[0];
    //}
    //else {
    //  dB[0] = 0;
    //}
    
    if(tx == 0) {
      clocks[batchid] = clock64()-start;
    }
  }

  extern "C" void
  kernel_vecDistCompute_DimPerThread(
    magma_int_t N, magma_int_t batchCount,
    float** dA_array, magma_int_t ldda, float** dB_array,
    magma_queue_t my_queue, int repetitive_time, long long *clocks)
  {
    const magma_int_t thread_x = N;
    dim3 threads(thread_x, 1, 1);
    dim3 grid(batchCount, 1, 1);
    cudaError_t e = cudaErrorInvalidValue;

    magma_int_t shmem  = 0;
    shmem += N * sizeof(float); // sD
    shmem += N * sizeof(float); // sX

    void *kernel_args[] = {&dA_array, &ldda, &dB_array, &repetitive_time, &clocks};
    switch(N){
      case  2: e = cudaLaunchKernel((void*)kernel_cuDistCompute< 2>, grid, threads, kernel_args, shmem, my_queue->cuda_stream()); break; 
      case 16: e = cudaLaunchKernel((void*)kernel_cuDistCompute<16>, grid, threads, kernel_args, shmem, my_queue->cuda_stream()); break;
      case 32: e = cudaLaunchKernel((void*)kernel_cuDistCompute<32>, grid, threads, kernel_args, shmem, my_queue->cuda_stream()); break;
      case 64: e = cudaLaunchKernel((void*)kernel_cuDistCompute<64>, grid, threads, kernel_args, shmem, my_queue->cuda_stream()); break;
      case 128: e = cudaLaunchKernel((void*)kernel_cuDistCompute<128>, grid, threads, kernel_args, shmem, my_queue->cuda_stream()); break;
      case 1024: e = cudaLaunchKernel((void*)kernel_cuDistCompute<1024>, grid, threads, kernel_args, shmem, my_queue->cuda_stream()); break;
      default: e = cudaErrorInvalidValue;
    }
    if( e != cudaSuccess ) {
        printf("cudaLaunchKernel of kernel_cuDistCompute is not successful!\n");
    }
  }

} // end of namespace

#endif
