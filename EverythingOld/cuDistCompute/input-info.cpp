#ifndef input_info_cpp_
#define input_info_cpp_
// ============================================================================
// Specifications of input-info.h
//
// Modifications
//    Chien  21-05-02:   Originally created
//
// ============================================================================
#include <stdio.h>
#include <iostream>
#include <fstream>
#include <iomanip>
#include "input-info.h"

namespace cmdInputs {
  // -- print usage --
  void print_usage()
  {
    std::cerr << "===============================================================================================================\n";
    std::cerr << "Usage: ./cuDistCompute-main <input-argument> <command>\n\n";
    std::cerr << "<input-argument> <command>\n"
                 "       -n        <integer>           # (or --number)  : number of points used for distance computation\n"
		 "       -k        <integer>           # (or --times)   : number of repetitive times\n"
                 "       -h                            # (or --help)    : print this help message\n\n";
    std::cerr << "----------------------- NOTICE -----------------------\n";
    std::cerr << "1. Order matters.\n";
    std::cerr << "2. The number of points must be greater than 1.\n";
    std::cerr << "3. The number of repetitive times must be greater or equal to 1\n";
    std::cerr << "----------------------- Examples -----------------------\n";
    std::cerr << "./cuDistCompute-main -n 100000 -k 10 # Compare 100000 pairwise distances 10 times repetitively.\n";
    std::cerr << "===============================================================================================================\n";
  }
}

#endif // input_info_cpp_
