#ifndef gpu_kernels_cu
#define gpu_kernels_cu
// =======================================================================
// CUDA kernels specification from header file gpu_kernels.h
//
// Modifications
//    Chien  21-05-10:   Originally Created
//    Chien  21-05-23;   Add repetition time
//    Chien  21-05-27:   Edit: One thread computes one dimension of distance
//    Chien  21-06-02:   Add clock counter to count time of each block (warp)
//
// =======================================================================
#include <stdio.h>
#include <stdlib.h>
#include <cstdio>
#include <iostream>
#include <iomanip>
#include <cstring>

// -- magma --
#include "flops.h"
#include "magma_v2.h"
#include "magma_lapack.h"
#include "magma_internal.h"
#undef max
#undef min
#include "magma_templates.h"
#include "sync.cuh"
#undef max
#undef min
#include "shuffle.cuh"
#undef max
#undef min
#include "batched_kernel_param.h"

// -- cuda included --
#include <cuda.h>
#include <cuda_runtime.h>

// -- headers --
#include "gpu_kernels.h"

namespace cuDistComputeWrapper {

  template<int N>
  __global__ void
  kernel_cuDistCompute_DistPerThread(
    float** dA_array, magma_int_t ldda,
    float** dB_array, int repetitive_time, long long *clocks)
  {
    //long long start=clock64();
    const int tx = threadIdx.x;
    const int batchid = blockIdx.x;

    // -- 1) declare registers and shared memories --
    // -- 1-1) registers and global memory address --
    float rA[N]  = {0.0};
    float rB[N]  = {0.0};
    float* dA = dA_array[batchid];
    float* dB = dB_array[batchid];

    // -- 1-2) shared memory --
    extern __shared__ float zdata[];
    float *sD = (float*)(zdata);
    float *sX = sD + 32;

    // -- 2) read data from global memory to registers --
    #pragma unroll
    for(int i = 0; i < N; i++){
        rA[i] = dA[ i*32 + tx ];
        rB[i] = dB[ i*32 + tx ];
    }
    
    // -- 3) repeat distance computation for repetitive_time times --
    //long long start;//=clock64();
    #pragma unroll
    for(int k = 0; k < repetitive_time; k++) {
      //start=clock64();
      sD[tx] = 0;
      sX[tx] = 0;
      //start=clock64();
      // -- compute distances over all dimensions --
      #pragma unroll
      for(int i = 0; i < N; i++){
	sD[tx] += (rA[i] - rB[i]) * (rA[i] - rB[i]);
      }
      __syncthreads();
      
      // -- take square root and store the distance in shared memory --
      sX[tx] = sqrt(sD[tx]);
      __syncthreads();
    }
    //if(tx == 0) {
    //  clocks[batchid] = clock64()-start;
    //}

    // -- 4) write the distance from shared memory back to global memory --
    dB[ tx ] = sX[tx];
    
    //#pragma unroll
    //for(int i = 0; i < N; i++){
    //    dA[ i + tx * N ] = rA[i];
    //}

    // -- do not count the time of thresholding --
    //if(sX[tx] < 1.4) {
    //  dB[ tx ] = sX[tx];
    //}
    //else {
    //  dB[ tx ] = 0;
    //}
    
    //if(tx == 0) {
    //  clocks[batchid] = clock64()-start;
    //}
  }

  extern "C" void
  kernel_vecDistCompute_DistPerThread(
    magma_int_t N, magma_int_t batchCount,
    float** dA_array, magma_int_t ldda, float** dB_array,
    magma_queue_t my_queue, int repetitive_time, long long *clocks)
  {
    const magma_int_t thread_x = 32;    
    dim3 threads(thread_x, 1, 1);
    dim3 grid(batchCount, 1, 1);
    cudaError_t e = cudaErrorInvalidValue;

    magma_int_t shmem  = 0;
    shmem += thread_x * sizeof(float); // sD
    shmem += thread_x * sizeof(float); // sX

    void *kernel_args[] = {&dA_array, &ldda, &dB_array, &repetitive_time, &clocks};
    switch(N){
      case  2: e = cudaLaunchKernel((void*)kernel_cuDistCompute_DistPerThread< 2>, grid, threads, kernel_args, shmem, my_queue->cuda_stream()); break; 
      //case 16: e = cudaLaunchKernel((void*)kernel_cuDistCompute_DistPerThread<16>, grid, threads, kernel_args, shmem, my_queue->cuda_stream()); break;
      //case 32: e = cudaLaunchKernel((void*)kernel_cuDistCompute_DistPerThread<32>, grid, threads, kernel_args, shmem, my_queue->cuda_stream()); break;
      //case 64: e = cudaLaunchKernel((void*)kernel_cuDistCompute_DistPerThread<64>, grid, threads, kernel_args, shmem, my_queue->cuda_stream()); break;
      case 128: e = cudaLaunchKernel((void*)kernel_cuDistCompute_DistPerThread<128>, grid, threads, kernel_args, shmem, my_queue->cuda_stream()); break;
      //case 1024: e = cudaLaunchKernel((void*)kernel_cuDistCompute_DistPerThread<1024>, grid, threads, kernel_args, shmem, my_queue->cuda_stream()); break;
      default: e = cudaErrorInvalidValue;
    }
    if( e != cudaSuccess ) {
        printf("cudaLaunchKernel of kernel_cuDistCompute is not successful!\n");
    }
  }

} // end of namespace

#endif
