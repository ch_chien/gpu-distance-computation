#ifndef gpu_kernels_h
#define gpu_kernels_h
// ============================================================================
// CUDA kernels header file
//
// Modifications
//    Chien  21-05-09:   Originally created
//
// ============================================================================
#include <iostream>
#include <fstream>
#include <chrono>
#include <ostream>
#include <vector>
#include <cmath>
#include <unordered_set>

// -- magma --
#include "magma_v2.h"
#include "magma_lapack.h"

extern "C" {
namespace cuDistComputeWrapper {

  // -- idea 1: one distance is computed by one thread --
  void kernel_vecDistCompute_DistPerThread(
    magma_int_t N, magma_int_t batchCount,
    float** dA_array, magma_int_t ldda, float** dB_array,
    magma_queue_t my_queue, int repetitive_time, long long *clocks
  );
  
  // -- idea 2: one dimension of distance is computed by one thread -- 
  void kernel_vecDistCompute_DimPerThread(
    magma_int_t N, magma_int_t batchCount,
    float** dA_array, magma_int_t ldda, float** dB_array,
    magma_queue_t my_queue, int repetitive_time, long long *clocks
  );

  // -- cpu version of distance computation --
  float const computeDistance(
    float* dataPointer, unsigned int const dimension, 
    unsigned int const index1, unsigned long int const index2
  );
  std::unordered_set<unsigned int> cpu_version(
    float* dataPointer, unsigned int const dimension, 
    unsigned long int const datasetSize, int repetitive_time
  );
  bool sets_residual(
    std::unordered_set<unsigned int> set_cpu,
    std::unordered_set<unsigned int> set_gpu
  );
}
}

#endif
