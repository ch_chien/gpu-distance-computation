#ifndef gpu_compute_cu
#define gpu_compute_cu
// =======================================================================
// GPU version of computing distances between two points, where one thread
// computes one distance (specifications of header file gpu_compute.cuh)
//
// Modifications
//    Chien  21-06-06:   Originally Created
//
// =======================================================================
#include <stdio.h>
#include <stdlib.h>
#include <cstdio>
#include <chrono>
#include <iostream>
#include <iomanip>
#include <cstring>
#include <unordered_set>
#include <algorithm>

// -- magma --
#include "flops.h"
#include "magma_v2.h"
#include "magma_lapack.h"
#include "magma_internal.h"
#undef max
#undef min
#include "magma_templates.h"
#include "sync.cuh"
#undef max
#undef min
#include "shuffle.cuh"
#undef max
#undef min
#include "batched_kernel_param.h"

// -- cuda included --
#include <cuda.h>
#include <cuda_runtime.h>

// -- headers --
#include "gpu_compute.cuh"
#include "gpu_kernels.h"

// -- cpu computation --
//#include "cpu_compute.h"

namespace cuDistComputeWrapper {

  void Cvt2GpuArray_DistPerThread(
    float* dataPointer, unsigned int const dimension, unsigned int const batchCount,
    int ref_Point, float *h_A, float *h_B)
  {
    /*for (int b = 0; b < batchCount; b++) {
      for (int i = 0; i < 32; i++) {
	for (unsigned int d = 0; d < dimension; d++)
	{
	  (h_A + b * 32 * dimension)[i*dimension + d] = dataPointer[ref_Point*dimension + d];
	  (h_B + b * 32 * dimension)[i*dimension + d] = dataPointer[(b*32+i+1)*dimension + d];  
	}
	//std::cout<<(h_B + b * 32 * dimension)[i*dimension]<<"\t"<<(h_B + b * 32 * dimension)[i*dimension+1]<<std::endl;
      }
      //std::cout<<std::endl;
    }*/
    
    for (int b = 0; b < batchCount; b++) {
      for (unsigned int d = 0; d < dimension; d++) {
        for (int i = 0; i < 32; i++)	
        {
          (h_A + b * 32 * dimension)[i + 32*d] = dataPointer[ref_Point*dimension + d];
          (h_B + b * 32 * dimension)[i + 32*d] = dataPointer[(b*32+i+1)*dimension + d];  
        }
	      //std::cout<<(h_B + b * 32 * dimension)[i*dimension]<<"\t"<<(h_B + b * 32 * dimension)[i*dimension+1]<<std::endl;
      }
      //std::cout<<std::endl;
    }
  }

  real_Double_t gpu_compute_dist_per_thread(
    float* dataPointer, unsigned int const dimension, unsigned long int const dataSize,
    int ref_Point, int repetitive_time)
  {
    std::unordered_set<unsigned int> set_gpu;

    magma_init();
    magma_print_environment();

    real_Double_t  gflops, gpu_perf, gpu_time;
    real_Double_t  time_cpu;
    float          *h_A, *h_B;
    magmaFloat_ptr d_A, d_B;
    magma_int_t N, lda, ldb, ldda, lddb, sizeA, sizeB;
    long long *clk_data;
    long long *host_clk_data;
    // -- write required clock-cycle time of each block --
    std::ofstream sm_time_file;
    sm_time_file.open("/users/cchien3/data/cchien3/MyBitBucket/gpu-nearest-neighbor/build/bin/block_clkcyc_time.txt");
    if (!sm_time_file.is_open())
       std::cout<<"sm time file cannot be opened!"<<std::endl;

    magma_int_t batchCount;
    float **dA_array = NULL;
    float **dB_array = NULL;

    magma_queue_t my_queue;    // magma queue variable, internally holds a cuda stream and a cublas handle
    magma_device_t cdev;       // variable to indicate current gpu id

    magma_getdevice( &cdev );
    magma_queue_create( cdev, &my_queue );     // create a queue on this cdev
    int peak_clk = 1;	       // measure clock time
    int device = 0;	       // device id used in get attribute instruction
    
    N          = dimension;
    //batchCount = dataSize-1;
    lda        = N;
    ldb        = lda;
    ldda       = magma_roundup( N, 32 );
    lddb       = ldda;
    batchCount = magma_ceildiv( dataSize-1, 32 );
    //gflops = ( FLOPS_SGETRF( N, 1 ) + FLOPS_SGETRS( N, 1 ) ) * batchCount / 1e9;

    sizeA = lda*32*batchCount;
    sizeB = ldb*32*batchCount;
    std::cout<<"batchCount = "<<batchCount<<std::endl;

    magma_smalloc_cpu( &h_A, sizeA );
    magma_smalloc_cpu( &h_B, sizeB );

    magma_smalloc( &d_A, lda*32*batchCount );
    magma_smalloc( &d_B, ldb*32*batchCount );
    
    // -- clock and gpu clock peak frequency --
    host_clk_data = (long long *)malloc(batchCount*sizeof(long long));
    cudaError_t err = cudaDeviceGetAttribute(&peak_clk, cudaDevAttrClockRate, device);
    err = cudaMalloc(&clk_data, batchCount*sizeof(long long));

    magma_malloc( (void**) &dA_array,    batchCount * sizeof(float*) );
    magma_malloc( (void**) &dB_array,    batchCount * sizeof(float*) );

    // -- convert the data pointer to h_A and h_B --
    Cvt2GpuArray_DistPerThread(dataPointer, dimension, batchCount, ref_Point, h_A, h_B);
    
    std::cout<<" -- original -- "<<std::endl;
    int s = 0;
    //magma_sprint(32, N, h_A + s * N * 32, 32);
    //magma_sprint(32, N, h_B + s * N * 32, 32);
    
    magma_ssetmatrix( 32, N*batchCount, h_A, 32, d_A, 32, my_queue );
    magma_ssetmatrix( 32, N*batchCount, h_B, 32, d_B, 32, my_queue );

    magma_sset_pointer( dA_array, d_A, ldda, 0, 0, 32*N, batchCount, my_queue );
    magma_sset_pointer( dB_array, d_B, lddb, 0, 0, 32*N, batchCount, my_queue );

    // ===================================================================
    // magma GPU cgesv batched solver
    // ===================================================================
    std::cout<<"GPU computing ..."<<std::endl;
    gpu_time = magma_sync_wtime( my_queue );
    kernel_vecDistCompute_DistPerThread( N, batchCount, dA_array, ldda, dB_array, my_queue, repetitive_time, clk_data);
    gpu_time = magma_sync_wtime( my_queue ) - gpu_time;
    gpu_perf = gflops / gpu_time;

    // -- check returns from the kernel --
    magma_sgetmatrix( 32, N*batchCount, d_B, 32, h_B, 32, my_queue );
    //magma_sprint(32, N, h_B + s * N * 32, 32);
    
    // -- transfer back to cpu; used to measure clock cycles and convert it into time --
/*    err = cudaMemcpy(host_clk_data, clk_data, batchCount*sizeof(long long), cudaMemcpyDeviceToHost);
    printf("peak clock rate: %dkHz\n", peak_clk);
    
    float block_time = 0.0;
    for (int b = 0; b < batchCount; b++) {
        block_time = host_clk_data[b]/(float)peak_clk;
	sm_time_file << b << "\t" << block_time << "\n";
    }
    sm_time_file.close();
*/    
    // -- store all non-zero indices to set_gpu --
    /*int arrow_indx = 0;
    for (int b = 0; b < batchCount; b++) {
      for (int i = 0; i < 32; i++) {
	arrow_indx = b * 32 + i;
        //if ((h_B + b * 32 * dimension)[i*dimension] != 0 && arrow_indx < dataSize-1) {
        //  set_gpu.insert(b * 32 + i + 1);
        //}
        if ((h_B + b * 32 * dimension)[i] != 0 && arrow_indx < dataSize-1) {
          set_gpu.insert(b * 32 + i + 1);
        }
      }
    }*/
    
    // -- cpu compute --
    time_cpu = magma_sync_wtime( my_queue );
    std::unordered_set<unsigned int> set_cpu = cpu_version(dataPointer,dimension, dataSize, repetitive_time);
    time_cpu = magma_sync_wtime( my_queue ) - time_cpu;
    
    // -- verify whether it is numerically correct for both cpu and gpu --
    //bool okay = sets_residual(set_cpu, set_gpu);
    bool okay = 1;

    // -- print out the test parameters and time --
    printf("%% DataSize   Dimensions   Repetitions   CPU time (msec)   GPU time (msec)   Residual\n");
    printf("%%=====================================================================================\n");
    std::cout<<"    "<<(long long) dataSize<<"         "<<(long long) N<<"          "<<repetitive_time<<"         ";
    std::cout<< time_cpu*1000<<"           "<< gpu_time*1000<<"            "<<(okay ? "ok" : "failed")<<std::endl;

    free( host_clk_data );
    magma_queue_destroy( my_queue );
    magma_free_cpu( h_A );
    magma_free_cpu( h_B );

    magma_free( d_A );
    magma_free( d_B );
    magma_free( dA_array );
    magma_free( dB_array );
    cudaFree( clk_data );
    fflush( stdout );
    printf( "\n" );
    magma_finalize();

    return gpu_time*1000;
  }
} // -- end of namespace --

#endif
