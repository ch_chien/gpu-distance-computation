#ifndef input_info_h_
#define input_info_h_
// ============================================================================
// Header file for declaring user input information functions
//
// Modifications
//    Chien  21-05-03:   Originally created
//
// ============================================================================

namespace cmdInputs {
  void print_usage();
}

#endif // input_info_h_
