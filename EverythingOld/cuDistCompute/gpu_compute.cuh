#ifndef gpu_compute_cuh
#define gpu_compute_cuh
// ============================================================================
// GPU version of computing distances between two points - header file
//
// Modifications
//    Chien  21-05-09:   Originally created
//
// ============================================================================
#include <stdio.h>
#include <stdlib.h>
#include <cstdio>
#include <chrono>
#include <iostream>
#include <iomanip>
#include <cstring>
#include <unordered_set>

// -- magma --
#include "magma_v2.h"
#include "magma_lapack.h"

namespace cuDistComputeWrapper {

  // -- One thread computes one distance, regardless of dimensions --
  real_Double_t gpu_compute_dist_per_thread(
    float* dataPointer, unsigned int const dimension, 
    unsigned long int const dataSize,
    int ref_Point, int repetitive_time
  );
  
  void Cvt2GpuArray_DistPerThread(
    float* dataPointer,
    unsigned int const dimension, unsigned int const batchCount,
    int ref_Point, float *h_A, float *h_B
  );

  // -- one thread computes one distance of one dimension --
  real_Double_t gpu_compute(
    float* dataPointer, unsigned int const dimension, 
    unsigned long int const dataSize,
    int ref_Point, int repetitive_time
  );

  void Cvt2GpuArray(
    float* dataPointer,
    unsigned int const dimension, unsigned int const batchCount,
    int ref_Point, float *h_A, float *h_B
  );
}

#endif
