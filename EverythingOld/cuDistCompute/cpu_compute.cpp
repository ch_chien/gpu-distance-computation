#ifndef cpu_compute_h
#define cpu_compute_h
// ============================================================================
// CPU version of computing distances between two points
//
// Modifications
//    Chien  21-05-07:   Originally created
//    Chien  21-05-23;   Add repetition time
//
// ============================================================================
#include <iostream>
#include <fstream>
#include <chrono>
#include <ostream>
#include <vector>
#include <cmath>
#include <unordered_set>
#include <algorithm>

// -- magma --
#include "magma_v2.h"
#include "magma_lapack.h"

// -- header file --
#include "gpu_kernels.h"

namespace cuDistComputeWrapper {

  // compute distance between two indices in float pointer
  extern "C" float const
  computeDistance(float* dataPointer, unsigned int const dimension, unsigned int const index1, unsigned long int const index2)
  {
      float distance = 0;
      for (unsigned int d = 0; d < dimension; d++)
      {
          float difference = (dataPointer[index1*dimension + d]) - (dataPointer[index2*dimension + d]);
          distance += difference * difference;
      }
      //return distance;
      return std::sqrt(distance);
  }

  // take reference point as first entry
  // compute distance to all other points
  // keep points within certain distance
  extern "C" std::unordered_set<unsigned int>
  cpu_version(float* dataPointer, unsigned int const dimension, unsigned long int const datasetSize, int repetitive_time)
  {
      std::unordered_set<unsigned int> set_cpu;
      float sqrt_dist = 0;
      for (int k = 0; k < repetitive_time; k++) {
	unsigned int const reference_point = 0;
	for (int i = 1; i < datasetSize; i++) {
	  float distance = 0;
	  distance = computeDistance(dataPointer, dimension, reference_point, i);
	  if (distance < 1.4)
	  {
              if(k == 0)
                  set_cpu.insert(i);
	  }
	}
      }

      return set_cpu;
  }
  
  extern "C" bool 
  sets_residual(std::unordered_set<unsigned int> set_cpu, std::unordered_set<unsigned int> set_gpu)
  {
    bool set_equal = 0;
    // -- print out elements within the sets --
    //std::cout<<"cpu set: "<<std::endl;
    //for (auto const &i: set_cpu) {
    //      std::cout << i << " ";
    //}
    //std::cout<<"\ngpu set: "<<std::endl;
    //for (auto const &i: set_gpu) {
    //      std::cout << i << " ";
    //}
    //std::cout<<std::endl;
    
    // -- verify whether it is numerically correct for both cpu and gpu -- 
    //std::cout << "Number of points in set in cpu: " << set_cpu.size() << std::endl;
    //std::cout << "Number of points in set in gpu: " << set_gpu.size() << std::endl;
    if (set_cpu.size() != set_gpu.size()) {
      std::cout<<"Sizes of both sets are not equal!"<<std::endl;
      return set_equal;
    }
    
    // -- copy the unordered set to a vector and sort the elements --
    std::vector<unsigned int> cpu_set_vec(set_cpu.begin(), set_cpu.end());
    std::vector<unsigned int> gpu_set_vec(set_gpu.begin(), set_gpu.end());
    
    // -- sort the elements of the vectorized set --
    std::sort(cpu_set_vec.begin(), cpu_set_vec.end());
    std::sort(gpu_set_vec.begin(), gpu_set_vec.end());
    //for (std::vector<unsigned int>::const_iterator i = cpu_set_vec.begin(); i != cpu_set_vec.end(); ++i) {
    //  std::cout << *i << ' ';
    //}
    //std::cout<<"\n"<<std::endl;
    
    // -- verify whether cpu set and gpu set have the same elements --
    if (cpu_set_vec == gpu_set_vec) {
      std::cout<<"Both sets are equal!"<<std::endl;
      set_equal = 1;
    }
    else {
      std::cout<<"Both sets are not equal!"<<std::endl;
      set_equal = 0;
    }
    
    return set_equal;
  }
}

#endif
